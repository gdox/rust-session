fn main() {
    let temp_c = Celsius(20.0);
    let temp_f = temp_c.to_fahrenheit();
    println!("The temperature in Fahrenheit is {}", temp_f.0);
}
