use std::net::{TcpListener, TcpStream};
use std::io;
use std::io::prelude::*;
use std::collections::HashSet;

fn handle_stream(mut stream : TcpStream) -> io::Result<()> {
    let mut set = HashSet::new();
    for line in io::BufReader::new(&mut stream).lines() {
        set.insert(line?);
    }
    for word in set {
        writeln!(stream, "You wrote {} at least once", word)?;
    }
    Ok(())
}

fn main() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8000")?;

    for stream in listener.incoming() {
        let _ = handle_stream(stream?);
    }
    Ok(())
}
