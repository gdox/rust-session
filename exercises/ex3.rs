use std::io;
use std::io::prelude::*;
use std::collections::HashSet;
use std::collections::HashMap;

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut set = HashSet::new();

    for line in stdin.lock().lines() {
        set.insert(line?);
    }

    for line in set {
        println!("You wrote {} at least once", line);
    }
    Ok(())
}
