fn polish_notation(input : &Vec<&str>) -> i64 {
    // TODO implement
    unimplemented!()
}

fn main() {
    let input = vec!['+', '*', '3', '5', '1'];
    assert_eq(polish_notation(&input), 16);

    let input = vec!['-', '*', '3', '5', '1'];
    assert_eq(polish_notation(&input), 14);

    let input = vec!['/', '+', '7', '2', '3'];
    assert_eq(polish_notation(&input), 3);

    let input = vec!['-', '5', '/', '8', '2'];
    assert_eq(polish_notation(&input), 1);
}
