# rust-session

Content for the Rust session on 20/21 February 2019

## Building

    cd exercises
    rustc ex1.rs

The Rust compiler has some interesting flags (`rustc --help`), but the most important one is the `-O` flag to enable optimizations.
